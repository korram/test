import React, { useState } from 'react';
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';
import Main from './Main';
import Test1 from './Test1';
import Test2 from './Test2';
import { Layout, Menu, theme } from 'antd';
const { Header, Sider, Content } = Layout;
const App = () => {
  const mainLayout = (
      <Main />
  );
  
  return (
  <BrowserRouter>
  <Routes>
    <Route path="/" element={mainLayout}>
      <Route path="test1" element={<Test1 />} />
      <Route path="test2" element={<Test2 />} />
    </Route>
  </Routes>
</BrowserRouter>);
};
export default App;

